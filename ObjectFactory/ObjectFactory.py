import importlib
import os
import logging
from utility_universal.logger import formatter
from SETTINGS import DEFAULT_ENTRYPOINT_METHOD, CLASS_NAME_SUFFIX, FILE_NAME_SUFFIX


class ObjectFactory:

    @staticmethod
    def setup_logger(package_name):
        """

        :param package_name:
        :return:
        """

        log_file_path = ObjectFactory.get_or_create_logging_directory(package_name)

        # initialize logger for the application

        logger = logging.getLogger(package_name)
        logger.setLevel(logging.DEBUG)
        handler = logging.FileHandler(log_file_path)
        handler.setFormatter(formatter)
        logger.addHandler(handler)

        return logger

    @staticmethod
    def get_or_create_logging_directory(package_name):

        """
        checks if logging directory and log file is existing

        if package_name is Line

        logging directory:  "messenger/logs/Line/Line.log"

        if the directory is not existing, the parent directory will be created and path is returned

        :param package_name: Name of the package which is under consideration
        :return: returns the path of log file (existing or created)

        """
        current_directory = os.path.dirname(__name__)
        parent_directory = os.path.dirname(current_directory)

        log_directory_path_arr = [parent_directory, 'logs', package_name]
        log_directory_path = os.path.join(*log_directory_path_arr)

        # create the parent directory, ignore if already existing
        os.makedirs(log_directory_path, exist_ok=True)

        log_file_path = os.path.join(log_directory_path, f'{package_name}.log')
        return log_file_path

    @staticmethod
    def get_obj_and_add_url_rule(app, config):

        """
        Attaches Url Rule to the entry method provided in config, if not, attacheds url
        url to default method provided in settings

        :param app: app context for the flask app (Application factory Model of flask)
        :param config: List of applications and their respective entry point urls
                [
                    {
                        "class": "Line.LineMessenger.LineMessenger",
                        "method": "process_message",  # optional
                        "url_rule": "/webhook"
                    }
                ]

        :return: None
        """

        for mess_config in config:

            package_name = mess_config.get('package_name', "")
            absolute_package_name = f'messenger.{package_name}'
            package_settings_path = f'{absolute_package_name}.settings'

            package_class_name_suffix = getattr(importlib.import_module(package_settings_path), 'CLASS_NAME_SUFFIX',CLASS_NAME_SUFFIX)
            package_file_name_suffix = getattr(importlib.import_module(package_settings_path), 'FILE_NAME_SUFFIX',FILE_NAME_SUFFIX)

            file_name = f'{package_name}{package_file_name_suffix}'
            class_name = f'{package_name}{package_class_name_suffix}'

            file_abs_path = f'{absolute_package_name}.{file_name}'

            cls = getattr(importlib.import_module(file_abs_path), class_name)
            local_logger = ObjectFactory.setup_logger(package_name)
            print(local_logger)
            obj = cls(logger=local_logger)
            method = getattr(obj, mess_config.get('method') or DEFAULT_ENTRYPOINT_METHOD)
            app.add_url_rule(
                mess_config.get('url_rule'),
                f"{str(cls)}.{mess_config.get('method',DEFAULT_ENTRYPOINT_METHOD)}",
                method,
                methods=mess_config.get("METHODS") or ['POST']
            )
            print("Added url {} for {} at {}".format(
                mess_config.get('url_rule'),
                mess_config.get('class'),
                method
            ))
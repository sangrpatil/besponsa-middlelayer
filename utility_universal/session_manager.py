import inspect
import os
import requests
import json


def create_server_session(user_id ,bot_id=None ,client=None):
    print("In crete Session", user_id, bot_id ,client)

    print(inspect.currentframe().f_code.co_name + ": Creating  session for USer: ", user_id,bot_id)
    server_host = os.environ.get('SERVER_HOST') or 'localhost'
    server_port = os.environ.get('SERVER_PORT') or '8000'

    get_request_url = "http://{server_host}:{server_port}/bot/{bot_id}/init?user_id={user_id}&client={client}".format(
        server_host=server_host,
        server_port=server_port,
        bot_id=bot_id,
        user_id=user_id,
        client=client
    )
    response = requests.get(get_request_url).text
    print(response)
    response = json.loads(response)
    print(inspect.currentframe().f_code.co_name + ": Session Created with  ", response)

    return response



# Sets and gets context of an user
from .mongo_dao import insert,get_by_id, update

# returns context_id
def set(context_json): #todo insert timestamp
    context_id = insert('context',context_json)
    return context_id

def modify(context_id, context_json):
    context_id = update('context', context_id, context_json)
    return context_id

# returns context_json
def get(context_id):
    context_json = get_by_id('context',context_id)
    return context_json






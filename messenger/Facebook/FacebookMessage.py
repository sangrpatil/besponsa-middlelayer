from flask import abort, request

import re
import os

from messenger.Base.MessageMixin import MessageMixin
from .settings import SERVER_CLIENT_FUNCTIONS_MAPPING as FUNCTION_MAPPINGS, PFIZER_LOGO
from SETTINGS import IMAGES_PATH, IMAGE_URL

import json


class FacebookMessage(MessageMixin):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._logger = kwargs.get('logger')

    def _get_sliced_text(self, text):
        pass

    def _url_message(self, item):

        self._logger.info(f"Process URL message")

        #response  = None

        urls = item['interaction elements']
        urls = urls.split('|')
        self._logger.info(f"URLS: {urls}")

        # if item['text']:
        #     text_palyload = {
        #         "message": {
        #             "text": item['text']
        #     }
        # }
        #
        # if text_palyload:
        #     response.append(text_palyload)

        if len(urls) == 1:

            url = urls[0]
            url_image = f"{IMAGE_URL}{PFIZER_LOGO}"
            url_image = "http://logok.org/wp-content/uploads/2014/11/Pfizer_logo-1024x768.png"

            self._logger.info(f"URL is {url}")
            self._logger.info(f"URL image is {url_image}")

            url_payload = {
                "message": {
                    "attachment": {
                        "type": "template",
                        "payload": {
                            "template_type": "generic",
                            "elements": [
                                {"title": item['text'],
                                 "image_url": url_image,
                                 # "subtitle": "We have the right hat for everyone.",
                                 "default_action": {
                                     "type": "web_url",
                                     "url": url,
                                     # "webview_height_ratio": "COMPACT",
                                     # "webview_height_ratio": "TALL",
                                     "webview_height_ratio": "FULL",
                                 },
                                 # "buttons": [{"type": "web_url", "url": URL, "title": "View Website"},{"type": "postback","title": "Start Chatting","payload": "DEVELOPER_DEFINED_PAYLOAD"}]
                                 }
                            ]
                        }
                    }
                }
            }
            self._logger.info(f"URL Payload: {url_payload}")
            return url_payload

    def _string_message(self, item):

        self._logger.info("Processing string message")

        text = self._clean(item['text'])

        payload = {
            "message": {
                "text": text
            }
        }

        return payload

    def _image_message(self, item):

        self._logger.info(" Processing image message")

        text = self._clean(item['text'])

        image = item['interaction elements'][0]

        payload = {
            "message": {
                "text": text,
                "attachment": {
                    "type": "image",
                    "payload": {
                        "url": image
                    }
                }
            }
        }

        return payload

    def _button_message(self, item):

        self._logger.info("processing button message")

        text = item['text']
        text = self._clean(text)

        payload = {
            "message": {
                "text": text,
                "quick_replies": list()

            }
        }

        actions = list()

        for button in item['interaction elements']:

            actions.append({
                "content_type": "text",
                "title": button,
                "payload": "",
                "image_url": ""
            })

        payload['message']['quick_replies'].extend(actions)

        return payload

    def _clean(self,text):

        self._logger.info("Cleaning text: "+ text)

        m = re.search(r'\d+$', text)
        cleaned = re.sub(r'\d+$', '', text)

        if m is not None:
            print("Converted Text before superscript change = {}".format(m.group(0)))
            sup = str.maketrans("0123456789", u"\u2070" + chr(0x00b9) + chr(0x00B2) + chr(
                0x00B3) + u"\u2074" + u"\u2075" + u"\u2076" + u"\u2077" + u"\u2078" + u"\u2079")

            return cleaned + m.group(0).translate(sup)

        else:
            return cleaned

    def process_message(self,message):

        self._logger.info("Entered Process Message funtion")

        try:
            if message['status'] == 'No response':
                print('Acknowledgement was success. Do no execute anything')
                return '0'
        except Exception as e:
            print('Exception in reading status = {}'.format(e))

        if not isinstance(message, dict) and not isinstance(message.get('result', dict()).get('message'), list):
            pass

        else:

            self._logger.info( " Started processing message")
            final_output = list()

            try:

                for item in message['result']['message']:

                    text = item['text']
                    text = self._clean(text)
                    self._logger.info("Received cleaned text: " + text)
                    print(item.get('interaction'))
                    print(" Interaction : " + item.get('interaction') )
                    print(FUNCTION_MAPPINGS.get(item.get('interaction')))
                    response = getattr(self, FUNCTION_MAPPINGS.get(item.get('interaction')))(item)
                    final_output.append(response)

            except Exception as e:
                self._logger.exception(e)

        self._logger.info(f"Final response : {json.dumps(final_output, indent=2)} ")

        return final_output







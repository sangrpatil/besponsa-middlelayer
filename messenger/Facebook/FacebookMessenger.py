from flask import abort, request
from .speechtotext import wavefile
from messenger.Base.MessengerMixin import MessengerMixin
from .FacebookMessage import FacebookMessage
from .settings import CONFIG, MESSAGE_CONFIG

import json
from copy import deepcopy


class FacebookMessenger(MessengerMixin):

    _type = 'facebook'

    def __init__(self, **kwargs):

        self._client_connection_config = CONFIG
        super().__init__(**kwargs)
        self._init_app(**kwargs)

    def _init_app(self, **kwargs):

        self._logger.info("Initializing Facebook Messenger Class")
        print(MESSAGE_CONFIG)
        self.fb_url = self._client_connection_config['FB_LINK']
        message_configs = deepcopy(MESSAGE_CONFIG)
        message_configs.update(dict(logger=self._logger))
        self._message_object = FacebookMessage(**message_configs)

    def _test_connection(self):
        print("Test Connect Called in Line Messenger")
        return True

    def _get_conn_obj(self):
        pass

    def _get_msg_object(self):
        return self._message_object


    def _send_reply_to_facebook(self,payload):

        self._logger.info(json.dumps(payload,indent=2))

        headers = {
            'content-type': "application/json"
        }

        self._post(url=self.fb_url,body=json.dumps(payload),headers=headers)

    def process_message(self):
        print( request)

        if request.method == "GET":
            token_sent = request.args.get("hub.verify_token")
            return self._verify_challenge_token(token_sent)

        data = request.json
        print('data =',data)
        self._logger.info("Entered Process Message [POST]")
        self._logger.info(json.dumps(data,indent=4))
        page_id = self._get_page_id(data)
        sender_id = self._get_sender_id(data)
        recipient_id = self._get_recipient_id(data)
        self._logger.info(f'Found Page ID: {page_id} \n sender_id: {sender_id} \n recipient id: {recipient_id}' )
        text = ''
        message = self._retrieve_message(data)
        print('message in messenger.py', message)
        if not message:
            return 'OK'
        try:
            print('inside try of messenger')
            mess = data['entry'][0]['messaging'][0]['message']
            #text_mess = data['entry'][0]['messaging'][0]['message']['text']
            print("message",mess)
            if mess['attachments'][0]['payload']['url'] !='':
                print('inside if inside try')
                url = mess['attachments'][0]['payload']['url']
                print('audio url is:',url)
                text = wavefile(url)
                print('converted text is:',text)
                #return text

        except Exception as e:
            print('inside except')
            print(str(e))
            if mess['text']:
                print('inside else')
                text = mess['text']
                print('text is',text)
        self._logger.info(f'retrieved message {message}')
        print('logger print is',self._logger.info(f'retrieved message {message}'))
        #replace message= message with message = text
        #response = self.send_message_to_server(type=FacebookMessenger._type, message=message, user_id=sender_id, page_id = page_id)
        response = self.send_message_to_server(type=FacebookMessenger._type, message=text, user_id=sender_id,
                                               page_id=page_id)
        print('response is:',response)
        self._logger.info("Revived response from server " + json.dumps(response,indent=2))

        processed_response = self._get_msg_object().process_message(response)


        # add sender id in response

        for item in processed_response:
            item.update(
            {
                "recipient": {
                    "id": sender_id
                }
            }
            )
            self._logger.info(json.dumps(item,indent=2))

            self._send_reply_to_facebook(item)


        return 'OK'


    def _verify_challenge_token(self,token):

        if token == self._client_connection_config['HUB_TOKEN']:
            return request.args.get("hub.challenge")
        return 'Invalid verification token'

    def _get_user(self,sender_id):

        url = self._client_connection_config['USER_URL']
        url = url.format(
            sender_id=sender_id,
            ACCESS_TOKEN=self._client_connection_config['ACCESS_TOKEN']
        )

        response = self._get(url=url)
        return response

    def _get_page_id(self,data):

        page_id = data['entry'][0]['id']
        return page_id

    def _get_sender_id(self,data):

        sender_id = data['entry'][0]['messaging'][0]['sender']['id']
        return sender_id

    def _get_recipient_id(self,data):

        sender_id = data['entry'][0]['messaging'][0]['recipient']['id']
        return sender_id

    def _retrieve_message(self,data):

        string_data = json.dumps(data)

        message = ""

        if 'postback' in string_data:
            message = data['entry'][0]['messaging'][0]['postback']
        elif 'message' in string_data:
            message = data['entry'][0]['messaging'][0]['message']

        if "text" in message:
            text = message['text']
        elif "payload" in message:
            text = message['payload']
        else:
            text = None

        if 'attachments' in message and 'payload' in message['attachments'][0]:
                print(message['attachments'][0])
                text = 'location'

        return text


    @property
    def fb_url(self):
        return self._fb_url

    @fb_url.setter
    def fb_url(self,url):
        print("Entered fb url setter")

        url = url.format(
            ACCESS_TOKEN=self._client_connection_config['ACCESS_TOKEN']
        )
        self._fb_url = url







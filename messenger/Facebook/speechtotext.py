import urllib.request as urllib2
#from urllib.request import urlopen, Request
import subprocess
import uuid
AUDIO_FILES_DIRECTORY = 'resources/data/recordings/'
import os
import io
from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types

def wavefile(url):
    mp4file = urllib2.urlopen(url)
    print('type is ',type(mp4file))

    filename = str(uuid.uuid4())
    filename_mp4 = AUDIO_FILES_DIRECTORY + filename + '.mp4'
    filename_wav = AUDIO_FILES_DIRECTORY + filename + '.wav'

    print("Mp4 filename: ", filename_mp4)
    print("wav filename: ", filename_wav)

    with open(str(filename_mp4), "wb") as handle:
        handle.write(mp4file.read())
        print(handle)
    cmdline = ['ffmpeg',
               '-i',
               str(filename_mp4),
               '-ac',
               '1',
               '-vn',
               str(filename_wav)]
    subprocess.call(cmdline, shell=True)
    converted_text=transcribe_file(str(filename_wav))
    print('inside speech to text.py converted text is:',converted_text)
    # os.remove(str(filename_mp4))
    # os.remove(str(filename_wav))
    return converted_text


#take speech wav file and turn it into text. return text
def transcribe_file(speech_file):
    """Transcribe the given audio file."""

    print("SPEECH FILE DIR = {}".format(speech_file))
    client = speech.SpeechClient()
    print('client', client)

    audio = types.RecognitionAudio()
    content = ''
    with io.open(speech_file, 'rb') as audio_file:
        content = audio_file.read()
    audio = types.RecognitionAudio(content=content)

    config = types.RecognitionConfig(
        encoding=enums.RecognitionConfig.AudioEncoding.LINEAR16,
        # sample_rate_hertz=16000,
        language_code='en-US')

    response = client.recognize(config, audio)
    # Each result is for a consecutive portion of the audio. Iterate through
    # them to get the transcripts for the entire audio file.
    print('test')
    for result in response.results:
        # The first alternative is the most likely one for this portion.
        print('Transcript: {}'.format(result.alternatives[0].transcript))
        return(result.alternatives[0].transcript)



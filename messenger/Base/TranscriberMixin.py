import os
import uuid

import base64
from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types

from SETTINGS import RECORDINGS_PATH

import pydub
from pydub import AudioSegment



class TranscriberMixin():

    def __init__(self,**kwargs):
        self._logger = kwargs.get("logger")

    def _transcribe_audio(self,data,ext):

        file_name = self._get_file_name(ext=ext)
        file_path = self._dump(file_name,data)

        return self._get_text_from_audio_file(file_path,ext)

    def _get_file_name(self,ext):

        _id = uuid.uuid4()
        class_name = self.__class__.__name__
        file_name = f"{class_name}_{_id}.{ext}"

        return file_name

    def _dump(self,file,data):

        self._logger.info(f"File: {file}")
        self._logger.info(f"Data: {data}")

        if file:

            file_path = os.path.join(RECORDINGS_PATH, file)

            with open(file_path,"wb") as binary_file:
                binary_file.write(data)

                self._logger.info(f"Data written successfully in file path: {file_path}")

            return file_path

    def _remove_file(self, filepath):

        self._logger.info(f"Removing file: {filepath}")

        if filepath and os.path.isfile(filepath):
                os.remove(filepath)

                self._logger.info(f"File {filepath} removed successfully")

    def _get_text_from_audio_file(self, speech_file, ext):

        """
        Speech to text conversion function.
         - Receives the name of the audio file which needs to be converted to text
         - converts to text using Google Cloud Api

        :param speech_file: name of the audio file
        :return: text form of audio received

        """

        path = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
        file_path = os.path.join(path, speech_file)


        self._logger.info(f"Path of the ifle {file_path}")

        audio_segment = AudioSegment.from_file(file_path, format=ext)
        raw_data = audio_segment.raw_data

        self._logger.info(f"Raw Data {raw_data}")

        client = speech.SpeechClient()
        audio = types.RecognitionAudio(content=raw_data)
        config = types.RecognitionConfig(
            encoding=enums.RecognitionConfig.AudioEncoding.LINEAR16,
            sample_rate_hertz=16000,
            language_code='en-US'
        )

        self._logger.info(f"Config {config}")

        response = client.recognize(config, audio)
        self._logger.info(f"Response received from google speech api {response}")

        self._remove_file(file_path)

        for result in response.results:
            # The first alternative is the most likely one for this portion.
            self._logger.info(f"Result alternatives: {result.alternatives}")
            self._logger.info(u'Transcript: {}'.format(result.alternatives[0].transcript))
            self._logger.info('Confidence: {}'.format(result.alternatives[0].confidence))

            return result.alternatives[0].transcript

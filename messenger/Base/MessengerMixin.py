from abc import ABCMeta, abstractmethod
from flask import current_app as app, url_for

import json
import requests
import os
import uuid

from SETTINGS import APP_HOST as SERVER_HOST, APP_PORT as SERVER_PORT

import base64
from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types

import pydub
from pydub import AudioSegment



class MessengerMixin(metaclass=ABCMeta):

    def __init__(self, **kwargs):
        print("Object Created of Messenger Mixin")
        self._logger = kwargs['logger']

    @abstractmethod
    def _test_connection(self):
        print("Test Connection Called in MessengerMixin Class")

    @abstractmethod
    def process_message(self):
        print("to be overriden MessengerMixin Class")

    def _get_middle_layer_request_url(self):
        url = url_for('webhook_function')
        print(url)

        post_request_url = "http://{server_host}:{server_port}{method}".format(
            server_host=SERVER_HOST,
            server_port=SERVER_PORT,
            method=url
        )
        return post_request_url

    def _post(self,**kwargs):
        print(kwargs)
        response = requests.request(
            "POST",
            kwargs['url'],
            data=kwargs['body'],
            headers=kwargs['headers']
        )
        response_json = json.loads(response.text)
        print("Response: ", response_json)
        self._logger.info(f"[ERROR]: {response.text}")

        try:
            if response_json['status'] == 'Success':
                # process the response returned from server
                return response_json

        except Exception as e:
            print("Exception ocourred: ", e)

    def _get(self,url):
        response = requests.get(
            url
        )
        return response.json

    def send_message_to_server(self, **kwargs):
        post_request_data = {
            "state": "",
            "ASR": False,
            "bot_id": app.config['BOT_ID']
        }
        post_request_data.update(kwargs)
        post_request_data = json.dumps(post_request_data)
        headers = {
            'content-type': "application/json"
        }
        post_request_url = self._get_middle_layer_request_url()
        return self._post(body=post_request_data, url=post_request_url, headers=headers)



    # def _dump(self,file,data):
    #
    #     self._logger.info(f"File: {file}")
    #     self._logger.info(f"Data: {data}")
    #     if file:
    #         with open(file,"wb") as binary_file:
    #             binary_file.write(data)
    #
    #         self._logger.info(f"Data written successfully in file: {file}")
    #
    # def _remove_file(self,filepath):
    #
    #     self._logger.info(f"Removing file: {filepath}")
    #     if filepath and os.path.isfile(filepath):
    #             os.remove(filepath)
    #
    #             self._logger.info(f"File {filepath} removed successfully")
    #
    # def _get_text_from_audio_file(self, speech_file,ext):
    #
    #     """
    #     Speech to text conversion function.
    #      - Receives the name of the audio file which needs to be converted to text
    #      - converts to text using Google Cloud Api
    #
    #     :param speech_file: name of the audio file
    #     :return: text form of audio received
    #
    #     """
    #
    #     path = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
    #     file_path = os.path.join(path, speech_file)
    #
    #     self._logger.info(f"Path of the ifle {file_path}")
    #
    #     audio_segment = AudioSegment.from_file(file_path, format=ext)
    #     raw_data = audio_segment.raw_data
    #
    #     self._logger.info(f"Raw Data {raw_data}")
    #
    #     client = speech.SpeechClient()
    #     audio = types.RecognitionAudio(content=raw_data)
    #     config = types.RecognitionConfig(
    #         encoding=enums.RecognitionConfig.AudioEncoding.LINEAR16,
    #         sample_rate_hertz=16000,
    #         language_code='en-US'
    #     )
    #
    #     self._logger.info(f"Config {config}")
    #
    #     response = client.recognize(config, audio)
    #     self._logger.info(f"Response received from google speech api {response}")
    #
    #     self._remove_file(file_path)
    #
    #     for result in response.results:
    #         # The first alternative is the most likely one for this portion.
    #         self._logger.info(f"Result alternatives: {result.alternatives}")
    #         self._logger.info(u'Transcript: {}'.format(result.alternatives[0].transcript))
    #         self._logger.info('Confidence: {}'.format(result.alternatives[0].confidence))
    #
    #         return result.alternatives[0].transcript

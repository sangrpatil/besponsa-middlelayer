from linebot.models import TextSendMessage, URIAction, MessageAction, ButtonsTemplate
from linebot.models import TemplateSendMessage, FlexSendMessage
from linebot.models import BubbleContainer, BoxComponent, TextComponent, ButtonComponent
from linebot.models import BubbleStyle, BlockStyle
from linebot.models import QuickReply, QuickReplyButton

from messenger.Line.settings import SERVER_CLIENT_FUNCTIONS_MAPPING as FUNCTION_MAPPINGS
from messenger.Base.MessageMixin import MessageMixin

import textwrap
import copy
import json


class LineMessage(MessageMixin):

    def __init__(self,**kwargs):
        
        self._show_only_flex_messages = kwargs['SHOW_ONLY_FLEX_MESSAGES']
        self._quick_replies = kwargs["SHOW_QUICK_REPLIES"],
        self._text_size = kwargs['TEXT_SIZE']

        super().__init__(**kwargs)
        self._logger.info("Initializing the Line messenger Child class")
        print("Initialized LineMessage Class")

    _get_sliced_text = lambda self,x: x[:self._max_text_length]

    _get_button_sliced_text = lambda self,x: x[:160]

    def _url_message(self,item):

        text = self._get_sliced_text(item.get("text", "") )
        resp = " ".join([text, item.get('interaction elements')])
        self._logger.info(json.dumps(resp, indent=4))

        uri_action = URIAction(
            label=text,
            uri=item.get('interaction elements')
        )
        template_message = ButtonsTemplate(
            text=text,
            actions=[
                uri_action
            ]
        )
        return TemplateSendMessage(
            alt_text=text,
            template=template_message
        )

    def _string_message(self,item):

        text = self._get_sliced_text(item.get("text", "") )
        resp = " ".join([text, item.get('interaction elements')])
        return TextSendMessage(text=resp)

    def _button_message(self,item):

        text = item.get("text","")
        resp = self._get_button_sliced_text(text)
        interaction_elements = item.get('interaction elements',[])

        if self._no_of_buttons:
            interaction_elements = interaction_elements[:self._no_of_buttons]

        actions = list()
        print("interaction elements", interaction_elements)
        for ele in interaction_elements:
            print(ele)

            if "," in ele:
                url, label = ele.split(',')
                uri_action = URIAction(
                    uri=url,
                    label=label
                )
                actions.append(uri_action)

            else:
                message_action = MessageAction(
                    label=ele[:self._max_button_label_length],
                    text=ele
                )
                actions.append(message_action)

            print(actions)

        template = ButtonsTemplate(
            text=resp,
            actions=actions

        )

        return TemplateSendMessage(
            alt_text=resp,
            template=template
        )

    def split_messages(self, messages):

        self._logger.info("Reshuffling the messages")

        wrapper = textwrap.TextWrapper(width=200)

        no_of_messages = len(messages)
        no_of_text_messages = no_of_messages

        button_messages = []
        for item in messages:
            if item['interaction'].split("_")[0] == "button":
                no_of_text_messages -= 1
                button_messages.append(copy.deepcopy(item))

            elif item['interaction'].split("_")[0] == "url":
                no_of_text_messages -= 1
                button_messages.append(copy.deepcopy(item))

        if no_of_text_messages == 0:
            return messages

        word_list = list()
        text_message_element = None

        for i in messages[: no_of_text_messages]:

            if i['interaction'] == "text":
                word_list.extend(wrapper.wrap(i['text']))

                if not text_message_element:
                    text_message_element = copy.deepcopy(i)

        no_of_button_messages = len(messages) - no_of_text_messages
        no_of_remaining_messages = 4 - no_of_button_messages

        text_messages = []
        for line in word_list[: no_of_remaining_messages]:
            item = copy.deepcopy(text_message_element)
            item['text'] = line
            text_messages.append(copy.deepcopy(item))

        new_messages = []
        new_messages.extend(text_messages)
        new_messages.extend(button_messages)

        return new_messages

    def _create_flex_message(self, message):

        self._logger.info("Creating flex message")

        reply_list = []
        buttons =[]

        for item in message:

            bubble_container = BubbleContainer(
                body=BoxComponent(
                    layout="vertical",
                    margin="md",
                    contents=[]
                ),
                style=BubbleStyle(footer=BlockStyle(separator=True))
            )

            text = item['text']

            body = BoxComponent(
                layout="vertical",
                contents=[
                    TextComponent(text=text, size=self._text_size, wrap=True, margin="md", spacing="sm")
                ]
            )

            contents = []
            if item['interaction'].split("_")[0] == "button":

                if not self._quick_replies:
                    if len(item['interaction elements']):
                        bubble_container.footer = BoxComponent(
                            layout="vertical",
                            contents=[]
                        )

                    for element in item['interaction elements']:

                        message_action = MessageAction(
                            label=element,
                            text=element
                        )

                        contents.append(ButtonComponent(action=message_action,
                                                        style="link",
                                                        height="sm",
                                                        gravity="center"
                                                        )
                                        )

                else:
                    for element in item['interaction elements']:
                        buttons.append(
                            QuickReplyButton(action=MessageAction(label=element, text=element), height="md")
                        )

            elif item['interaction'] == "url":

                if len(item['interaction elements']):
                    bubble_container.footer = BoxComponent(
                        layout="vertical",
                        contents=[]
                    )

                    url_action = URIAction(
                        label=item['interaction elements'],
                        uri=item['interaction elements']
                    )

                    contents.append(ButtonComponent(action=url_action,
                                                    style="secondary")
                                    )

            bubble_container.body = body

            if contents:
                bubble_container.footer.contents.extend(contents)

            response = FlexSendMessage(
                alt_text="Message",
                contents=bubble_container
            )

            reply_list.append(response)
            if buttons:

                reply_list[-1].quick_reply = QuickReply(items=buttons, height="xl")

        return reply_list

    def process_message(self,message):

        try:
            print("Processing message in class Line Message")
            self._logger.info(f"Flex message flag {self._show_only_flex_messages}")

            if self._show_only_flex_messages:
                response = self._create_flex_message(message.get("result").get('message'))
                print(response)

                return response

            final_output = []

            new_messages = self.split_messages(message.get("result").get('message'))

            for item in new_messages:
                print(item.get('interaction'))
                self._logger.info(item.get('interaction'))
                self._logger.info(FUNCTION_MAPPINGS.get(item.get('interaction')))
                print(FUNCTION_MAPPINGS.get(item.get('interaction')))
                response = getattr(self,FUNCTION_MAPPINGS.get(item.get('interaction')))(item)
                final_output.append(response)

            # self._logger.info(json.dumps(final_output, indent=4))
            print(final_output)
            return final_output

        except Exception as e:
            self._logger.exception(e)

from messenger.Base.MessageMixin  import MessageMixin
from .settings import SERVER_CLIENT_FUNCTIONS_MAPPING as FUNCTION_MAPPINGS


class WebMessage(MessageMixin):

    def __init__(self,**kwargs):
        super().__init__(**kwargs)
        print("Initialized WebMessage Class")
        self._logger.info("Initialized WebMessage Class")

    _get_sliced_text = lambda self,x: x[:self._max_text_length]

    def _url_message(self,item):
        self._logger.info(f"Processing item {item}")
        return item

    def _string_message(self,item):
        self._logger.info(f"Processing item {item}")
        return item

    def _button_message(self,item):
        self._logger.info(f"Processing item {item}")
        return item

    def process_message(self,message):
        # print("Processing message in class Line Message")
        # self._logger.info("Processing message ")
        # final_output = []
        #
        # for item in message.get("result").get('message'):
        #     print(item.get('interaction'))
        #     self._logger.info(f"Interacation is {item.get('interaction')}, calling {FUNCTION_MAPPINGS.get(item.get('interaction'))}")
        #     print(FUNCTION_MAPPINGS.get(item.get('interaction')))
        #     response = getattr(self,FUNCTION_MAPPINGS.get(item.get('interaction')))(item)
        #     final_output.append(response)


        #self._logger.info(f"Final output {final_output}")

        #print(final_output)
        return message



